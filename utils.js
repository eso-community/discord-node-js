/* eslint-disable */
const fs = require('fs');
const path = require('path');
const format = require('dateformat');
const con = require('./db');
const ESO = require('./esoActivity');
const Twitch = require('./twitch');
const Discord = require('discord.js');
const constants = require('./constants');
const { log, logError } = require('./logger');
const liveChannel = process.env.DISCORD_CHANNEL_ID_TWITCH;
const epChannel = process.env.DISCORD_CHANNEL_ID_EP;
let lastRandom = null;
// Use lowercase only, to avoid typos
const bannedTwitchChannels = [];

class Utils {
  /**
   * Updates the online presence of verified esoc members on db
   * @returns {Promise<void>}
   */
  static async updateDiscordUserPresence(client) {
    log('Updating user presence on db...');
    const esocGuild = client.guilds.find('name', process.env.GUILD);
    setInterval(() => {
      con.execute(constants.VERIFIED_ESOC_USER_QUERY)
        .then(result => {
          Promise.all(result[0].map(row => {
            const user = esocGuild.members.get(`${row.id}`);
            if (user !== undefined) {
              con.execute(constants.DISCORD_PRESENCE_UPDATE_QUERY,
                [user.presence.status, format(Date.now(), 'yyyy-mm-dd hh:MM:ss'), user.id]);
            }
          }))
            .catch(e => logError(`CANNOT UPDATE PRESENCE: ${e}`));
        });
    }, constants.DISCORD_PRESENCE_TIMEOUT);
  }

  /**
   * Gets valuable configurations parameters from the database
   * @return {Promise<void>} void
   */
  static async getConfig() {
    try {
      log('Getting configs from ESOC Database...');
      const key = await con.execute(constants.JWT_QUERY);
      process.env['SECRET_JWT_KEY'] = key[0][0].config_value;
      log('JWT secret key retrieved successfully!');
    } catch (error) {
      logError(`Error retrieving JWT key: ${error}`);
    }
  }

  /**
   * Returns name of the banned twitch streamers
   * @returns {Promise<void>}
   */
  static async updateTwitchBanList() {
    try {
      log('Getting list of banned twitch streamers on twitch');
      await con.execute(constants.TWITCH_BANNED_LIST_QUERY)
        .then(res => res[0].map(curr => bannedTwitchChannels.push(curr['channel_name'])));
    } catch (error) {
      logError(`Error retrieving list of banned twitch streamers : ${error}`);
    }
  }

  /**
   * Deletes embeds of the games/streams which are not active anymore
   * @param deleteJobs List of embeds to be deleted
   * @return {Promise<void>} Promise
   */
  static async deleteRedundantMessages(deleteJobs) {
    log('Delete redundant messages');
    if (deleteJobs.length >= 1) {
      await Promise.all(deleteJobs)
        .catch(error => console.error(`Failed to delete redundant messages. Error: ${error}`));
    }
  }

  /**
   * Converts array into a JS Object
   * @param array Input Array
   * @param keyField Field which will be used as the key
   * @return {*} Object from the input array
   */
  static arrayToObject(array, keyField) {
    return array.reduce((obj, item) => {
      obj[item[keyField]] = item;
      return obj;
    }, {});
  }

  /**
   *  Gets data from Twitch helix API and posts in the channel.
   *  All the streams are stored in map ({streamer_name: message_id}).
   *  Uses map to create/update/delete embed messages in the channel.
   *
   * @param client
   * @return Prints streams on the live-channel
   */
  static async startGettingStreams(client) {
    log('Start Getting Streams...');
    this.updateTwitchBanList();
    let streamEmbeds = new Map();
    const channel = client.channels.get(liveChannel);
    if (channel === undefined) return;  // for guilds which do not have live-stream channel

    log('Delete 100 last messages in stream channel...');
    try {
      channel.bulkDelete(100, true)
        .then(() => log('Delete successful'))
        .catch(error => logError(`Failed to delete messages. Error: ${error}`));
    } catch (e) {
    }
    setInterval(async () => {
      const tempStreamMap = new Map();
      log('Twitch get/refresh streams..');
      try {
        const response = await Twitch.getStream();
        log('Found streams');
        const streams = response.streams;
        await Promise.all(streams.map(async (stream) => {
          const user = await response.users[`user_${stream.user_id}`];
          if (bannedTwitchChannels.includes(stream.user_name.toLowerCase())) {
            log(`Stream user "${stream.user_name}" is banned by ESOC. Excluding it from the list.`);
            return;
          }
          log('Create embed..');
          const embed = await Twitch.createEmbed(response, stream, user);
          log('Created embed successfully');
          // Update the streams if changed
          if (streamEmbeds.get(user.display_name) !== undefined) {
            log(`Stream "${user.display_name}" updated. Edit discord message..`);
            const m = await channel.fetchMessage(streamEmbeds.get(user.display_name));
            tempStreamMap.set(user.display_name, m.id);
            m.edit('', { embed })
              .catch(logError);
            log(`"${user.display_name}" stream updated successfully`);
          }
          // Adds the stream if not in the map
          if (streamEmbeds.get(user.display_name) === undefined) {
            log(`Adding stream "${user.display_name}" to channel..`);
            const m = await channel.send({ embed });
            log(`"${user.display_name}" stream added successfully`);
            tempStreamMap.set(user.display_name, m.id);
          }
        }))
          .catch(error => logError(`Failed to update the streams messages. Error: ${error}`));
        // Deletes the streams if not found in the response
        const deleteStreams = [];
        streamEmbeds.forEach((val, key, map) => {
          if (map !== undefined && tempStreamMap.get(key) === undefined) {
            log(`Delete streams from discord that are not in the response from Twitch`);
            channel.fetchMessage(val)
              .then((message) => {
                deleteStreams.push(message.delete());
              })
              .catch(error => logError(`Failed to fetch message to be deleted ${error}`));
          }
        });
        this.deleteRedundantMessages(deleteStreams)
          .catch(logError);
        streamEmbeds = tempStreamMap;
      } catch (e) {
        logError(e);
      }
      log(`${constants.UPDATE_TWITCH_INTERVAL / 1000} seconds until next stream update...`);
    }, constants.UPDATE_TWITCH_INTERVAL);
  }

  /**
   * Gets the list of all maps unknown to the ESOC database
   * @return {Set<any>} Maps which are not recognized by the bot
   */
  static getUnknownMaps() {
    log('Getting unknown maps from maps_name.json');
    try {
      const mapSet = new Set(JSON.parse(fs.readFileSync('maps_name.json', 'utf8')));
      log('Found unknown maps set successfully');
      return mapSet;
    } catch (err) {
      log('Could not find maps_name.json, using empty set..');
      return new Set();
    }
  }

  /**
   * Starts fetching the EP games from the ESOC API
   * @param client Discord client
   * @return {Promise<void>} Posts games in embeds with their information
   */
  static async startGettingGames(client) {
    log('Start getting games...');
    const maps = await Utils.getMaps()
      .catch(logError);
    let unknownMaps = Utils.getUnknownMaps();
    let gameEmbeds = new Map();
    const channel = client.channels.get(epChannel);
    if (channel === undefined) return;  // for guilds which do not have eso-ep-activity channel

    try {
      channel.bulkDelete(100, false)
        .then(() => log('Delete successful'))
        .catch(error => logError(`Failed to delete messages. Error: ${error}`));
    } catch (e) {
    }
    setInterval(async () => {
      log('Get/refresh ESOC games...');
      const newGames = new Map();
      try {
        const games = await ESO.getLobbies();
        await Promise.all(games.map(async (game) => {
          log(`Creating embed for game "${game.name}"`);
          const embed = await ESO.createEmbed(game, maps, constants.MAP_SETS, unknownMaps);
          log('Successfully created an embed for ESOC game');

          // Update
          if ((gameEmbeds.get(game.id) !== undefined)) {
            log(`Update discord message for game ${game.id}`);
            const message = await channel.fetchMessage(gameEmbeds.get(game.id))
              .catch(error => logError(`Failed to fetch message with game id ${game.id}. Error: ${error}`));
            newGames.set(game.id, message.id);
            message.edit('', { embed })
              .catch(error => logError(`Failed to edit message with game id ${game.id}. Error: ${error}`));
            log(`Game "${game.name}" was updated`);
          }

          // Add
          if (gameEmbeds.get(game.id) === undefined) {
            log(`Add discord message for new game with id ${game.id}`);
            const message = await channel.send({ embed })
              .catch(error => logError(`Failed to send message embed for game "${game.id}". error: ${error}`));
            log(`Game "${game.name}" was created`);
            newGames.set(game.id, message.id);
          }
        }))
          .catch(error => logError(`Failed in Promise.all trying to create discord message embeds for all games. Error: ${error}`));

        // Remove
        const deleteGames = [];
        log('Remove old messages with games that are no longer hosted');
        gameEmbeds.forEach(async (val, key, map) => {
          if (map !== undefined && newGames.get(key) === undefined) {
            const message = await channel.fetchMessage(val)
              .catch(error => logError(`Failed to fetch message with value ${val}. Error: ${error}`));
            deleteGames.push(message.delete());
          }
        });
        this.deleteRedundantMessages(deleteGames)
          .catch(logError);
        gameEmbeds = newGames;
      } catch (e) {
        logError(e);
      }
      log(`Wait for ${constants.UPDATE_INTERVAL_ESOC / 1000} seconds before updating game list again...`);
    }, constants.UPDATE_INTERVAL_ESOC);
  }

  /**
   * Generates pseudo random number
   * @param max High cap of random number
   * @return {number|*} A random number
   */
  static random(max) {
    let random;
    if (lastRandom === undefined) {
      random = Math.floor(Math.random() * max);
    } else {
      random = Math.floor(Math.random() * max);
      if (random >= lastRandom) random += 1;
    }
    lastRandom = random % max;
    return lastRandom;
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Welcome message
   * @param member Newly joined member
   * @return {string} Welcome message
   */
  static getMessage(member) {
    const templates = [
      `Wololo! ${member} has been converted to ESOC`,
      `A new villager has been trained. Welcome ${member}!`,
      `${member} found a new treasure and joins the ESOC Discord.`,
      `${member} I said hi!`,
      `${member} joined, #modthe${member}!`,
      `${member}, did you know quick search games are always rated?`,
    ];
    const idx = this.random(templates.length);
    return templates[idx];
  }

  /**
   * Fetches Avatars from ESOC DB
   * @return {Promise<*>} Array containing avatar links
   */
  static async fetchAvatarsFromDb() {
    let avatar = [];
    try {
      log('Fetching avatars from database...');
      [avatar] = await con.execute(constants.AVATAR_QUERY);
      log('Successfully fetched avatars from database');
    } catch (error) {
      logError(`Failed to fetch avatars from database. ${error}`);
    }
    avatar = avatar.map(({ image_name, ...others }) => ({
      ...others,
      imageName: image_name
    }));
    return this.arrayToObject(avatar, 'hash');
  }

  /**
   * Fetches Maps from ESOC DB
   * @return {Promise<{mapName}[]>}
   */
  static async getMaps() {
    let maps = [];
    try {
      log('Fetching maps from database...');
      [maps] = await con.execute(constants.MAPS_QUERY);
      log('Successfully fetched maps from database');
    } catch (error) {
      logError(`Failed to fetch maps from database. ${error}`);
    }

    // Turn map_name into mapName
    return maps.map(({ map_name, ...others }) => ({ mapName: map_name, ...others }));
  }

  /**
   * Ensures the permissions of the muted role
   * @param guild
   * @return {Promise<*>}
   */
  static async ensureMutedRolePermissions(guild) {
    log('Ensure that muted role permissions are correctly set...');
    const mutedRole = guild.roles.find('name', 'Muted');

    if (!mutedRole) {
      return logError(`No mutedRole exists in ${guild.name}, and failed to create one.`);
    }

    try {
      return await Promise.all(Array.from(guild.channels, async ([channelId, channel]) => {
        return await channel.overwritePermissions(mutedRole, {
          SEND_MESSAGES: false,
          ADD_REACTIONS: false,
        });
      }));
    } catch (error) {
      return logError(`Failed to set channel permissions of mutedRole in ${guild.name}. Error: ${error}`);
    }
  }

  /**
   * Creates muted role and checks for the permissions
   * @param guild
   * @return {Promise<void>}
   */
  static async createMutedRole(guild) {
    log(`Create muted role in ${guild.name}`);
    await guild.createRole({
      name: 'Muted',
      color: constants.RED,
      permissions: [],
    })
      .catch(error => console.error(`Failed to create mutedRole in ${guild.name}. Error: ${error}`));

    await Utils.ensureMutedRolePermissions(guild);
  }

  /**
   * Ensures the existence of muted role, makes one if it doesn't exist
   * @param guilds
   * @return {Promise<void>}
   */
  static async ensureRolesExists(guilds) {
    Promise.all(Array.from(guilds, async ([guildId, guild]) => {
      const mutedRole = guild.roles.find('name', 'Muted');
      if (!mutedRole) {
        Utils.createMutedRole(guild)
          .catch(logError);
      }
      if (!constants.ESOC_GUILDS.includes(guild.name)) return;
      try {
        const verifiedRoles = guild.roles.findAll('name', constants.VERIFIED_ROLE);
        verifiedRoles.map(verifiedRole => verifiedRole.delete(`Deleting role— ${constants.VERIFIED_ROLE}`)
          .then(`Role ${constants.VERIFIED_ROLE} deleted`)
          .catch(logError));
      } catch (e) {
        logError(e);
      }
      log(`Creating role— ${constants.VERIFIED_ROLE}`);
      Utils.createVerifiedRole(guild);
    }))
      .catch(logError);
  }

  /**
   * Removes muted role from users
   * @param guilds
   * @param mutedUsers
   * @return {Promise<void>}
   */
  static async unmuteUsers(guilds, mutedUsers) {
    log('Make sure all users that should be unmuted have been unmuted...');
    await Promise.all(Object.entries(mutedUsers)
      .map(async ([userId, user]) => {
        if (Date.now() > user.unmuteAt) {
          const guild = guilds.get(user.guildId);
          const mutedRole = guild.roles.find('name', 'Muted');
          await guild.members.get(userId)
            .removeRole(mutedRole)
            .catch(error => logError(`Failed to remove muted role from user. Error: ${error}`));
          delete mutedUsers[userId];
        }
      }))
      .catch(error => logError(`Failed to unmute users. Error: ${error}`));
    log('Save users to mutedUsers.json...');
    Utils.writeJson(mutedUsers, path.join(__dirname, './data/mutedUsers.json'));
  }

  static createVerifiedRole(guild) {
    if (!constants.ESOC_GUILDS.includes(guild.name)) return;
    log(`Creating verified role in ${guild.name}`);
    guild.createRole({
      name: constants.VERIFIED_ROLE,
      color: constants.VERIFIED_BLUE,
      permissions: [],
      hoist: true,
    })
      .then(role => {
        con.execute(constants.VERIFIED_ESOC_USER_QUERY)
          .then(results => {
            results[0].map(result => {
              try {
                const user = guild.members.get(`${result.id}`);
                if (user === undefined) return;
                user.addRole(role)
                  .catch(logError);
              } catch (e) {
                logError(`Can't add verified role to ${result.name} in Guild: ${guild.name}\n ${e}`);
              }
            });
          })
          .catch(logError);
      })
      .catch(logError);
  }

  static async checkVerifiedUsers(guilds) {
    setInterval(() => {
      Promise.all(Array.from(guilds, async ([guildId, guild]) => {
        const role = guild.roles.find('name', constants.VERIFIED_ROLE);
        try {
          con.execute(constants.VERIFIED_ESOC_USER_QUERY)
            .then(results => {
              results[0].map(result => {
                try {
                  const user = guild.members.get(`${result.id}`);
                  if (user === undefined) return;
                  user.addRole(role)
                    .catch(logError);
                } catch (e) {
                  logError(`Can't add verified role to ${result.name} in Guild: ${guild.name}\n ${e}`);
                }
              });
            });
        } catch (e) {
          logError(e);
        }
      }))
        .catch(logError);
    }, constants.UPDATE_TWITCH_INTERVAL);
  }

  static writeJson(json, filePath) {
    // filePath: Absolute path to file, including filename
    try {
      fs.writeFileSync(filePath, JSON.stringify(json, null, 2));
    } catch (error) {
      logError(`Failed to write json. Error: ${error}`);
    }
  }

  static readJson(filePath, fallback) {
    // filePath: Absolute path to file, including filename
    try {
      return JSON.parse(fs.readFileSync(filePath, 'utf8'));
    } catch (error) {
      if (fallback !== undefined) {
        return fallback;
      }

      throw new Error(`Failed to parse/read json file, error: ${error}`);
    }
  }

  static getCommands() {
    let commands = new Discord.Collection();
    const commandObject = require('./commands/');

    Object.entries(commandObject)
      .forEach(
        ([key, value]) =>
          commands.set(key, value)
      );

    return commands;
  }

  static isOnlyDigits(input) {
    const onlyDigitsRegexp = new RegExp(/^[0-9]+$/);
    return onlyDigitsRegexp.test(input);
  }

  static mentionToUserId(mention) {
    return mention.replace(/[<@!>]/g, '');
  }
}

module.exports = Utils;

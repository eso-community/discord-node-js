const AVATAR_QUERY = 'SELECT hash, image_name FROM esoc.eso_avatar';
const BLUE = 0x117EA6;
const DARK_ORANGE = 0xFF470F;
const DEFAULT_AVATAR = '0c182d86-f9e0-4208-8074-0ce427e40a84';
const DISCORD_LOOKUP = 'SELECT id, name, discriminator FROM esoc.discord_users ed'
  + ' INNER JOIN phpBB.p_users pu ON ed.user_id = pu.user_id WHERE pu.username_clean =';
const GOLD = 0xffA500;
const GOLD_COUNT = 25;
const GRAY = 0x4F545C;
const GREEN = 3842351; // decimal representation of hexa color
const ESOC_SEARCH_RESULT = 'ESOC Database Search Result';
const ESOC_GUILDS = ['ESO-Community.net', 'ESOC Dev Team', 'Personal'];
const ESO_POP = 'http://agecommunity.com/_server_status_/';
const ESO_QUERY = 'http://www.agecommunity.com/query/query.aspx?g=age3y&name=arg1&md=user';
const ESOC = 'https://eso-community.net';
const ESOC_AUTH = 'discord';
const ESOC_AVATARS = '/images/avatars';
const ESOC_UPLOADED_AVATARS = '/download/file.php?avatar=';
const ESOC_IMAGES = '/images/aoe3';
const ESOC_LOBBIES_URI = '/assets/patch/api/lobbies.json';
const VERIFIED_ESOC_USER_QUERY = 'SELECT du.* FROM esoc.discord_users du';
const ESOC_LOOKUP = 'SELECT u.user_id, u.username, u.user_posts, '
  + 'u.user_avatar, u.user_avatar_type, u.user_regdate FROM phpBB.p_users u '
  + 'INNER JOIN esoc.discord_users e on e.user_id = u.user_id WHERE e.id = ';
const ESOC_SUPREMACY_STANDARD_LADDER = `${ESOC}/ladder.php?patch=official&type=treaty&mode=overall&player=`;
const ESOC_SUPREMACY_TREATY_LADDER = `${ESOC}/ladder.php?patch=esoc&type=supremacy&mode=overall&player=`;
const ESOC_PROFILE = `${ESOC}/memberlist.php?mode=viewprofile&u=`;
const ESOC_PATCH_ICON = `${ESOC_IMAGES}/patch-esoc-icon.png`;
const ESOC_XPMOD_ICON = `${ESOC_IMAGES}/tad-icon.png`;
const ESOC_PATCH_EMBED_COLOR = 0xc32025;
const ESOC_AVATAR_TYPE_UPLOAD = 'avatar.driver.upload';
const ESOC_AVATAR_TYPE_LOCAL = 'avatar.driver.local';
const ESOC_AVATAR_TYPE_REMOTE = 'avatar.driver.remote';
const JWT_QUERY = 'SELECT p.config_value FROM phpBB.p_config p WHERE p.config_name = \'jwt_secret\'';
const MAP_SETS = [
  {
    mapName: 'Unknown',
    DisplayName: 'Unknown',
    MiniMapUrl: `${ESOC_IMAGES}/maps/unknown.png`,
  },
  {
    mapName: 'Large Maps',
    DisplayName: 'Large Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/large_maps.png`,
  },
  {
    mapName: 'Asian Maps',
    DisplayName: 'Asian Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/asian_maps.png`,
  },
  {
    mapName: 'All Maps',
    DisplayName: 'All Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/all_maps.png`,
  },
  {
    mapName: 'ESOC Team Maps',
    DisplayName: 'ESOC Team Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/team_maps.jpg`,
  },
  {
    mapName: 'Team Maps',
    DisplayName: 'Team Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/team_maps.jpg`,
  },
  {
    mapName: 'KnB Maps',
    DisplayName: 'KnB Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/kb_maps.png`,
  },
  {
    mapName: 'ESOC Maps',
    DisplayName: 'ESOC Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/esoc_maps.jpg`,
  },
  {
    mapName: 'Classic Maps',
    DisplayName: 'Classic Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/classic_maps.png`,
  },
  {
    mapName: 'Standard Maps',
    DisplayName: 'Standard Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/standard_maps.png`,
  },
  {
    mapName: 'Tournament Maps',
    DisplayName: 'Tournament Maps',
    MiniMapUrl: `${ESOC_IMAGES}/maps/tournament_maps.png`,
  },
];
const MAPS_QUERY = 'SELECT a.alias_id, a.name AS map_name, m.*, p.username FROM esoc.maps_alias a '
  + 'INNER JOIN esoc.maps m ON m.ID = a.alias_id '
  + 'LEFT JOIN phpBB.p_users p ON p.user_id = m.Author';
const ORANGE = 0xFFA500;
const RED = 0xB22222;
const SCENARIO_IMAGE = `${ESOC_IMAGES}/maps/scenario.png`;
const TREATY_PATCH_ICON = `${ESOC_IMAGES}/patch-treaty-icon.png`;
const TREATY_PATCH_EMBED_COLOR = 0x0378c0;
const TWITCH = 'https://www.twitch.tv/';
const TWITCH_API_URI = 'https://api.twitch.tv/helix/';
const TWITCH_CLIENT_ID = 'l075cyh5nw7b2savfoc46nleqh2sg6';
const TWITCH_API_USERS_URI = 'users?id=';
const TWITCH_API_STREAMS_URI = 'streams?game_id=10819';
const TWITCH_OPTIONS = {
  headers: {
    'Client-ID': TWITCH_CLIENT_ID,
  },
  json: true,
  timeout: '30',
};
const UNKNOWN_MAP_IMAGE = `${ESOC_IMAGES}/maps/unknown.png`;
const UPDATE_TWITCH_INTERVAL = 60000; // milli seconds
const UPDATE_INTERVAL_ESOC = 15000; // milli seconds
const XP_MOD_EMBED_COLOR = 0xc27c0e;
const YELLOW = 15204220;
const VERIFIED_BLUE = 0x3498DB;
const VERIFIED_ROLE = 'Verified ESOC Member';
const TWITCH_BANNED_LIST_QUERY = 'SELECT channel_name FROM esoc.`streams_live` WHERE `banned` = 1';
const DISCORD_PRESENCE_UPDATE_QUERY = 'UPDATE esoc.discord_users t SET t.presence=?, t.presence_last_update=? WHERE t.id=?';
const DISCORD_PRESENCE_TIMEOUT = 60000;
const GET_PLAYER_DETAILS_QUERY = 'SELECT du.id, lpp.power_rating, lpe.elo_rating FROM esoc.ladder_player lp LEFT JOIN esoc.discord_users du ON du.user_id = lp.user_id LEFT JOIN esoc.ladder_player_power lpp ON lpp.player_id = lp.id AND lpp.type = ? LEFT JOIN esoc.ladder_player_elo lpe ON lpe.player_id = lp.id AND lpe.type = lpp.type AND lpe.patch = ? AND lpe.mode = ? WHERE lp.name IN (?)';

module.exports = {
  GET_PLAYER_DETAILS_QUERY,
  TWITCH_BANNED_LIST_QUERY,
  DISCORD_PRESENCE_UPDATE_QUERY,
  DISCORD_PRESENCE_TIMEOUT,
  VERIFIED_ROLE,
  VERIFIED_BLUE,
  AVATAR_QUERY,
  BLUE,
  DARK_ORANGE,
  DEFAULT_AVATAR,
  DISCORD_LOOKUP,
  GOLD,
  GOLD_COUNT,
  GRAY,
  GREEN,
  ESO_POP,
  ESO_QUERY,
  ESOC,
  VERIFIED_ESOC_USER_QUERY,
  ESOC_SEARCH_RESULT,
  ESOC_AUTH,
  ESOC_AVATARS,
  ESOC_GUILDS,
  ESOC_UPLOADED_AVATARS,
  ESOC_IMAGES,
  ESOC_LOBBIES_URI,
  ESOC_XPMOD_ICON,
  ESOC_LOOKUP,
  ESOC_PATCH_ICON,
  ESOC_PATCH_EMBED_COLOR,
  ESOC_PROFILE,
  ESOC_SUPREMACY_TREATY_LADDER,
  ESOC_SUPREMACY_STANDARD_LADDER,
  ESOC_AVATAR_TYPE_UPLOAD,
  ESOC_AVATAR_TYPE_LOCAL,
  ESOC_AVATAR_TYPE_REMOTE,
  JWT_QUERY,
  MAP_SETS,
  MAPS_QUERY,
  ORANGE,
  RED,
  SCENARIO_IMAGE,
  TREATY_PATCH_ICON,
  TREATY_PATCH_EMBED_COLOR,
  TWITCH,
  TWITCH_API_URI,
  TWITCH_CLIENT_ID,
  TWITCH_API_USERS_URI,
  TWITCH_API_STREAMS_URI,
  TWITCH_OPTIONS,
  UPDATE_TWITCH_INTERVAL,
  UPDATE_INTERVAL_ESOC,
  UNKNOWN_MAP_IMAGE,
  XP_MOD_EMBED_COLOR,
  YELLOW,
};

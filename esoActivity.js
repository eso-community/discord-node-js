const request = require('request-promise');
const fs = require('fs');
const Discord = require('discord.js');
const constants = require('./constants');
const con = require('./db');
const { log, logError } = require('./logger');

const { escapeMarkdown } = Discord.Util;

class EsoActivity {
  static async getLobbies() {
    const url = `${constants.ESOC}${constants.ESOC_LOBBIES_URI}`;
    log(`Fetching ESOC patch lobbies from ${url}...`);
    const options = {
      uri: url,
      headers: {
        'User-Agent': 'ESOC-Bot Discord/2.1',
      },
    };
    const req = await request(options)
      .catch(error => logError(`Failed to fetch lobbies from ${url}. Error: ${error}`));
    try {
      log('Parse lobbies..');
      const lobbies = JSON.parse(req);
      log('Successfully fetched and parsed ESOC patch lobbies');
      return lobbies;
    } catch (error) {
      logError(`Found JSON from ${url} but failed to parse it. Invalid JSON. Error: ${error}. \n JSON received: ${req}`);
      return [];
    }
  }

  static getUserLink(player, patch) {
    if (patch === 2) {
      return `${constants.ESOC_SUPREMACY_STANDARD_LADDER}${player}`;
    }
    return `${constants.ESOC_SUPREMACY_TREATY_LADDER}${player}`;
  }

  static getPatch(patch) {
    switch (patch) {
      case 1:
        return 'ESOC Patch';
      case 2:
        return 'Treaty Patch';
      case 3:
        return 'XP Mod';
      case 4:
        return 'Smackdown Patch';
      case 5:
        return 'Treaty Smackdown';
      case 6:
        return 'XP Mod Smackdown';
      default:
        return '';
    }
  }

  static getPatchIcon(patch) {
    // Note: No icons needed for 5-6, those should not be used anyway
    log('Get patch icon...');
    switch (patch) {
      case 1:
        log('Return patch icon for ESOC Patch');
        return `${constants.ESOC}${constants.ESOC_PATCH_ICON}`;
      case 2:
        log('Return patch icon for Treaty Patch');
        return `${constants.ESOC}${constants.TREATY_PATCH_ICON}`;
      case 3:
        log('Return patch icon for XPMOD Patch');
        return `${constants.ESOC}${constants.ESOC_XPMOD_ICON}`;
      case 4:
        log('Return patch icon for ESOC Patch');
        return `${constants.ESOC}${constants.ESOC_PATCH_ICON}`;
      default:
        logError(`Could not identify patch from value ${patch}.. Cannot pick a patch icon`);
        return null;
    }
  }

  static getEmbedColor(patch) {
    switch (patch) {
      case 1:
        return constants.ESOC_PATCH_EMBED_COLOR;
      case 2:
        return constants.TREATY_PATCH_EMBED_COLOR;
      case 3:
        return constants.XP_MOD_EMBED_COLOR;
      case 4:
        return constants.ESOC_PATCH_EMBED_COLOR;
      default:
        return constants.GRAY;
    }
  }

  static getDetailedGameMode(game) {
    if (game.game_mode === 0) {
      if (game.treaty_time !== 0) {
        let mode = `Treaty ${game.treaty_time} min.`;
        if (game.no_blockade) mode += ' No blockade';
        return mode;
      }
      if (game.koth) return 'King Of the Hill';
      return 'Supremacy';
    }
    if (game.game_mode === 1) return 'Deathmatch';
    return 'Not-found';
  }

  static getGameType({ game_mode: mode, treaty_time: tTime }) {
    if (mode === 0) {
      if (tTime !== 0) return 'Treaty';
      return 'Supremacy';
    }
    return 'Custom';
  }

  static getMap(map, patch) {
    log(`Convert map name "${map}" if possible`);

    if (patch === 1 || patch === 3) { // ESOC PATCH OR XP MOD
      switch (map) {
        case 'featured':
          return 'ESOC Team Maps';
        case 'fastrandom':
          return 'ESOC Standard Maps';
        case 'Largerandommaps':
          return 'RE Standard Maps';
        case 'randommaps':
          return 'Team Maps';
        case 'asianrandom':
          return 'ESOC Maps';
        case 'tournamentrandom':
          return 'Tournament Maps';
        default:
          return map;
      }
    } else if (patch === 2) { // TREATY PATCH
      if (map === 'Largerandommaps') return 'UI 2.2 TR Blitz Maps';
      if (map === 'asianrandom') return 'Asian Maps';
      if (map === 'Fastrandom') return 'Standard Maps';
    }
    return map;
  }

  static async getMapIcon(map, game, maps, mapSets, unknownMaps) {
    if (game.scenario) {
      const mapIcon = `${constants.ESOC}${constants.SCENARIO_IMAGE}`;
      log(`Game is scenario, get scenario image: ${mapIcon}`);
      return mapIcon;
    }
    const mapName = map.trim();
    let mapObject;
    try {
      // Finding map in the map database
      log(`Try to find map image for map "${mapName}"...`);
      mapObject = Object.entries(maps)
        .find(map => map[1].mapName.toLowerCase()
          .includes(mapName.toLowerCase()) || mapName.toLowerCase()
          .includes(map[1].mapName.toLowerCase()))[1];
      const teamPlayers = Math.floor(game.max_players / 2);
      return `${constants.ESOC}${mapObject.ImagesFolder}minimap/${teamPlayers}v${teamPlayers}_1.png`;
    } catch (error) {
      log(`Failed to find map image for map "${mapName}" in maps. Trying in map-sets.`);
      try {
        // Searching if the map is a map-set
        mapObject = Object.entries(mapSets)
          .find(map => map[1].mapName.toLowerCase()
            .includes(mapName.toLowerCase()) || mapName.toLowerCase()
            .includes(map[1].mapName.toLowerCase()))[1];
        return `${constants.ESOC}${mapObject.MiniMapUrl}`;
      } catch (error) {
        logError(`Failed to find map image for map "${mapName}". Something had an unexpected format. Error: ${error}`);
        if (!unknownMaps.has(mapName)) {
          log(`map "${mapName}" not found in maps_name.json`);
          try {
            log(`Add "${mapName}" to maps_name.json`);
            unknownMaps.add(mapName);
            fs.writeFile('maps_name.json', JSON.stringify([...unknownMaps], null, 2), (error) => {
              if (error) {
                logError(`Failed to write maps_name.json with new map ${mapName}. Error: ${error}`);
              } else {
                log(`Added "${mapName}" to maps_name.json successfully`);
              }
            });
          } catch (error) {
            logError(`Failed to add ${mapName} to maps_name.json. Error: ${error}`);
          }
        }
        return `${constants.ESOC}${constants.UNKNOWN_MAP_IMAGE}`;
      }
    }
  }

  static getLadderPatch(patch) {
    switch (patch) {
      case 1:
        return 'ESOC';
      case 2:
        return 'Treaty';
      default:
        return 'ESOC';
    }
  }

  static getTitle(rating) {
    const pr = Math.ceil(rating);
    if (pr <= 2) return 'Conscript';
    if (pr >= 3 && pr <= 7) return 'Pvt';
    if (pr >= 8 && pr <= 10) return 'LCorp';
    if (pr >= 11 && pr <= 13) return 'Corp';
    if (pr >= 14 && pr <= 16) return 'Sgt';
    if (pr >= 17 && pr <= 19) return 'MSgt';
    if (pr >= 20 && pr <= 22) return '2Lt';
    if (pr >= 23 && pr <= 25) return '1Lt';
    if (pr >= 26 && pr <= 28) return 'Capt';
    if (pr >= 29 && pr <= 31) return 'Maj';
    if (pr >= 32 && pr <= 34) return 'LtCol';
    if (pr >= 35 && pr <= 37) return 'Col';
    if (pr >= 38 && pr <= 40) return 'Brig';
    if (pr >= 41 && pr <= 43) return 'MajGen';
    if (pr >= 44 && pr <= 46) return 'Lt Gen';
    if (pr >= 47 && pr <= 49) return 'Gen';
    if (pr >= 50) return 'FMarshal';
    return '🤔';
  }

  static getGameMode({ max_players: players }) {
    const playerOnEachTeam = Math.floor(players / 2);
    return `${playerOnEachTeam}v${playerOnEachTeam}`;
  }

  static async getPlayerNameDiscordAndRating(game) {
    return game.players.reduce(async (acc, player) => {
      let str = await acc;
      if (player === null) return str;
      str = `${str}**${player}**`;

      // Getting player discord tag, pr and elo
      try {
        const [rows] = await con.execute(constants.GET_PLAYER_DETAILS_QUERY, [
          this.getGameType(game),
          this.getLadderPatch(game.patch),
          this.getGameMode(game),
          player]);
        str = `${str} ${rows[0].id === null ? '' : `(<@${rows[0].id}>)`} `
          + `(${rows[0].power_rating === null ? 'Conscript' : this.getTitle(rows[0].power_rating)}`
          + `/ ${rows[0].elo_rating === null ? '0' : Math.round(rows[0].elo_rating)})`;
      } catch (e) {
        log(`${player} not found in the ESOC Ladder.`);
      }
      return `${str}\n`;
    }, Promise.resolve(''));
  }

  static async createEmbed(game, maps, mapSets, unknownMaps) {
    let map = this.getMap(game.map, game.patch);
    log(`Map name: ${map}`);
    if (map && map[0] === map[0].toLowerCase()) {
      map = map[0].toUpperCase() + map.slice(1);
      log(`Capitalize map name: "${map}"`);
    }
    const thumbnailUrl = await this.getMapIcon(map, game, maps, mapSets, unknownMaps);
    const authorIconUrl = await this.getPatchIcon(game.patch);
    const playersWithRating = await this.getPlayerNameDiscordAndRating(game);
	let embedTitle = escapeMarkdown(game.name);
	
	if(game.password)
		embedTitle = '🔒' + embedTitle;
	
    return {
      title: embedTitle,
      url: game.quicksearch ? '' : this.getUserLink(game.players[0], game.patch),
      color: this.getEmbedColor(game.patch),
      thumbnail: {
        url: thumbnailUrl,
      },
      image: {
        url: '',
      },
      author: {
        name: this.getPatch(game.patch),
        icon_url: authorIconUrl,
      },
      fields: [
        {
          name: `Players (PR/${EsoActivity.getGameMode(game)} ELO)`,
          value: playersWithRating,
          inline: false,
        },
        {
          name: 'Map',
          value: escapeMarkdown(map),
          inline: true,
        },
        {
          name: 'Game mode',
          value: this.getDetailedGameMode(game),
          inline: true,
        },
        {
          name: 'Max Players',
          value: game.max_players,
          inline: true,
        },
      ],
    };
  }
}

module.exports = EsoActivity;

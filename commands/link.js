const jwt = require('jsonwebtoken');
const { ESOC, ESOC_AUTH, BLUE } = require('./../constants');
const { log } = require('./../logger');

const validate = (args) => {
  if (args.length !== 0) return [false, 'No argument is required for this command'];
  return [true];
};

module.exports = {
  name: 'link',
  description: 'Returns Discord account of forum user or forum account of Discord user',
  usage: '',
  async execute(message, args) {
    const isValid = validate(args);
    if (isValid[0] === false) {
      message.channel.send(isValid[1]);
      return;
    }

    const user = {
      id: message.author.id,
      name: message.author.username,
      discriminator: message.author.discriminator,
      avatar: message.author.avatar,
    };
    const token = jwt.sign(user, process.env.SECRET_JWT_KEY);
    log(`Link generated for ${user.name}#${user.discriminator} <@${user.id}>: `
      + `${ESOC}/${ESOC_AUTH}/${token}`);
    const embed = {
      title: 'Click me to link your discord account with ESOC',
      description: '',
      url: `${ESOC}/${ESOC_AUTH}/${token}`,
      color: BLUE,
      fields: [],
    };
    message.author.send({ embed });

    if (message.channel.type !== 'dm') {
      message.channel.send(`${message.author}, I have sent you a token, check DM!`);
    }
  },
};

const { prefix } = require('../config');
const { logError } = require('./../logger');
const Utils = require('../utils');

module.exports = {
  name: 'help',
  description: 'List all of my commands or info about a specific command.',
  aliases: ['commands'],
  usage: '[command name]',
  execute(message, args) {
    const commands = Utils.getCommands();

    if (!args.length) {
      const data = commands.map(command => `${prefix}${command.name}`)
        .join('\n');

      const msg = '```diff\n'
        + 'Here\'s the list of all my commands\n'
        + `${data}\n`
        + `You can send \`${prefix}help [command name]\` to get info on a specific command!\`\`\``;

      message.author.send(msg)
        .then(() => {
          if (message.channel.type === 'dm') return;
          message.reply('I\'ve sent you a DM with all my commands!');
        })
        .catch((error) => {
          logError(`Could not send help DM to ${message.author.tag}.\n`, error);
          message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
        });
      return;
    }

    const name = args[0].toLowerCase();
    const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

    if (!command) {
      message.reply('that\'s not a valid command!');
      return;
    }

    const data = [];
    data.push(`**Name:** ${command.name}`);

    if (command.aliases !== undefined) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
    if (command.description !== undefined) data.push(`**Description:** ${command.description}`);
    if (command.usage !== undefined) data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);

    message.channel.send(data, { split: true });
  },
};

const con = require('../db');
const { log, logError } = require('../logger');
const constants = require('../constants');
const { mentionToUserId } = require('../utils');
const { isOnlyDigits } = require('./../utils');

function validate(message, args) {
  if (args.length > 1) {
    message.channel.send('Only 1 user needs to be mentioned.');
    return false;
  }

  if (args.length < 1) {
    message.channel.send('You have to mention a user to lookup in ESOC database.');
    return false;
  }
  return true;
}

module.exports = {
  name: 'lookup',
  description: 'Looks up the [username] in the ESOC Database',
  usage: '[Forum name/ Discord profile]',
  async execute(message, args) {
    if (validate(message, args) === false) return;
    const id = mentionToUserId(args[0]);

    const errorEmbed = {
      title: constants.ESOC_SEARCH_RESULT,
      description: 'User not found!',
      thumbnail: {
        url: `${constants.ESOC}${constants.ESOC_PATCH_ICON}`,
      },
      color: constants.RED,
      fields: [
        {
          name: '**Haven\'t yet signed up on https://eso-community.net?**',
          value: 'Do it now! @ https://eso-community.net/ucp.php?mode=register',
        },
        {
          name: '**Haven\'t yet linked your discord account with ESOC?**',
          value: 'Send me `!link` as DM and I give you a SUPER SECRET TOKEN, just for you ❤',
        },
      ],
    };

    // search user in the forums
    if (isOnlyDigits(id)) {
      let userID;
      let result;
      try {
        log(`Searching ${args[0]}'s forum name in the database...`);
        result = await con.execute(`${constants.ESOC_LOOKUP}${id}`);
        log('Search complete');
        userID = result[0][0].user_id;
      } catch (error) {
        logError(error);
      }
      if (userID === undefined) {
        message.channel.send({ embed: errorEmbed });
        return;
      }

      result = result[0][0];
      const { username } = result;
      const userPosts = result.user_posts;
      let userAvatar = result.user_avatar;
      const userAvatarType = result.user_avatar_type;
      const userRegDate = new Date(result.user_regdate * 1000);

      if (userAvatarType === constants.ESOC_AVATAR_TYPE_LOCAL) {
        userAvatar = `${constants.ESOC}${constants.ESOC_AVATARS}/${userAvatar}`;
      } else if (userAvatarType === constants.ESOC_AVATAR_TYPE_UPLOAD) {
        userAvatar = `${constants.ESOC}${constants.ESOC_UPLOADED_AVATARS}${userAvatar}`;
      }
      const embed = {
        title: constants.ESOC_SEARCH_RESULT,
        url: `${constants.ESOC_PROFILE}${userID}`,
        description: '',
        thumbnail: {
          url: userAvatar,
        },
        color: constants.BLUE,
        fields: [
          {
            name: 'Username',
            value: username,
          },
          {
            name: 'User Posts',
            value: userPosts,
          },
          {
            name: 'Profile Link',
            value: `${constants.ESOC_PROFILE}${userID}`,
          },
          {
            name: 'User Since',
            value: userRegDate.toUTCString(),
          },
        ],
      };
      message.channel.send({ embed });
    } else {
      try {
        log('Looking for user in discord...');
        const query = `${constants.DISCORD_LOOKUP} '${args[0].toLowerCase()}'`;
        const result = await con.execute(query);
        const user = message.guild.members.get(result[0][0].id.toString());
        const embed = {
          title: constants.ESOC_SEARCH_RESULT,
          description: `User found: ${user}`,
          thumbnail: {
            url: user.user.avatarURL,
          },
          color: constants.BLUE,
          fields: [
            {
              name: 'Tag',
              value: `${user.user.tag}`,
            },
            {
              name: 'User since',
              value: new Date(user.user.createdAt).toUTCString(),
            },
          ],
        };
        if (user !== undefined) {
          message.channel.send({ embed });
        } else {
          // if user is not found in the database
          message.channel.send({ embed: errorEmbed });
        }
      } catch (error) {
        logError(error);
        // if user is not in the discord server
        message.channel.send({ embed: errorEmbed });
      }
    }
  },
};

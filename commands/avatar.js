const { logError } = require('../logger');

function validate(message, avatarURL) {
  let user = message.member;
  let hasRole = false;
  let seemsLegitUrl = false;

  user.roles.some(r=>{if(['Developers', 'Emoji Maker', 'Development Team', ].includes(r.name))
      hasRole = true;
  });

  if (avatarURL !== undefined)
    seemsLegitUrl = true;

  if (hasRole && seemsLegitUrl)
    return true;

}

module.exports = {
  name: 'avatar',
  description: 'Allow any mod to change the bot avatar.',
  execute(message, args) {
    if (validate(message, args[0]))
      message.client.user.setAvatar(args[0]).catch(logError);
  }
};

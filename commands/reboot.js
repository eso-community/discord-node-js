const OS_VAR = process.platform;
const shell = require('shelljs');
const { log } = require('./../logger');

module.exports = {
  name: 'reboot',
  description: 'Reboots the bot, command available to selected few.',
  execute(message) {
    if (message.author.username === 'Ashvin') {
      message.channel.send('REBOOTING THE BOT...')
        .then(() => {
          log('Rebooting EB...');
          let cmd = 'pm2 restart discord-bot';
          if (OS_VAR !== 'win32') cmd = `sudo ${cmd}`;
          shell.exec(cmd);
        });
    }
  },
};
